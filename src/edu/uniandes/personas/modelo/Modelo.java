package edu.uniandes.personas.modelo;

public interface Modelo {
	
	public Persona[] darPersonas();
	
	public void cambiarPersona(int pos, String nombre, int edad);

}
