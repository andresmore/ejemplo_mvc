package edu.uniandes.personas.vista;

import java.util.Scanner;

public class MainEjemploEntradaSalida {
	
	public static void main(String[] args) {
		
		
		
		new MainEjemploEntradaSalida().menuInicio();
		
		
		
		
		
		
		
	}
	
	
	public void menuInicio(){
		boolean salir=false;
		
		while(!salir){
			System.out.println("-----Ejemplo de salida-----");
			System.out.println("-----Men� de aplicaci�n----");
			System.out.println("1. Saludar");
			System.out.println("2. Salir");
			
			Scanner sc= new Scanner(System.in);
			try{
				int entrada=sc.nextInt();
				
				switch (entrada) {
				case 1:
					System.out.println("Digite su nombre");
					
					String nombre=sc.next();
					System.out.println("Hola "+nombre);
					break;

				case 2:
					salir=true;
					System.out.println("Salir para continuar ");
					sc.next();
					break;
				default:
					break;
				}
			}catch(Exception e){
				System.out.println("Opci�n inv�lida");
			}

		}
	}

}
